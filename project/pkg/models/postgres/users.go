package postgres

import (
	"aitu.com/project/pkg/models"
	"context"
	"database/sql"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"
)

const  (
	stmt = "INSERT INTO users (name, email, password) VALUES($1, $2, $3)"
	getUsers="SELECT id, password FROM users WHERE email = $1"


)
type UserModel struct {
	Pool *pgxpool.Pool
}
func (m *UserModel) Insert(name, email, password string) error {

		var id uint64
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
		row := m.Pool.QueryRow(context.Background(),stmt,name, email, string(hashedPassword))
		err = row.Scan(&id)
		if err != nil {
		return err
	}
		return nil
	}





func (m *UserModel) Authenticate(email, password string) (int, error) {
	var id uint64
	var hashedPassword []byte

	row := m.Pool.QueryRow(context.Background(), getUsers, email)
	err := row.Scan(&id, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil{
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword){
			return 0, models.ErrInvalidCredentials
		}else{
			return 0, err
		}
	}

	return int(id), nil
}

// We'll use the Get method to fetch details for a specific user based
// on their user ID.
func (m *UserModel) Get(id int) (*models.User, error) {
	return nil, nil
}

