package postgres

import (
	"aitu.com/project/pkg/models"
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	insertSql = "INSERT INTO product (name,status,description,price) VALUES ($1,$2,$3,$4) RETURNING id"
	getProductById = "SELECT id,name,status,description,price FROM product WHERE id=$1"
	getProducts = "SELECT id,name,status,description,price FROM product ORDER BY id"
	)


type ProductModel struct {
	Pool *pgxpool.Pool
}


func (m *ProductModel) Insert(name, status, description, price string) (int, error) {
	var id uint64
	err := m.Pool.QueryRow(context.Background(),insertSql,name,status,description, price).Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}


func (m *ProductModel) Get(id int) (*models.Product, error) {
	p := &models.Product{}
	err := m.Pool.QueryRow(context.Background(),getProductById,id).
		Scan(&p.ID,&p.Name,&p.Status,&p.Description,&p.Price)
	if err != nil {
		if err.Error() == "no rows in result set" {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return p, nil
}


func (m *ProductModel) Latest() ([]*models.Product, error) {
	products := []*models.Product{}
	rows, err := m.Pool.Query(context.Background(),getProducts)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		p := &models.Product{}
		err = rows.Scan(&p.ID, &p.Name, &p.Status, &p.Description, &p.Price)
		if err != nil {
			return nil, err
		}
		products = append(products, p)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return products, nil
}
