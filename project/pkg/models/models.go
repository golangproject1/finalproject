package models

import (
	"errors"
)

var (
	ErrNoRecord = errors.New("models: no matching record found")
	ErrInvalidCredentials = errors.New("models: invalid credentials")
	ErrDuplicateEmail = errors.New("models: duplicate email")
)


type Product struct {
	ID int
	Name string
	Status string
	Description string
	Price uint64
}
type User struct {
	ID int
	Name string
	Email string
	HashedPassword []byte

}
